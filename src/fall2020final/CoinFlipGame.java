package fall2020final;

import java.util.*;

public class CoinFlipGame {
	private int total;
	private int bet;
	
	public CoinFlipGame(int total, int bet) {
		this.total = total;
		this.bet = bet;
	}
	
	public int getTotal() {
		return total;
	}
	
	public int getBet() {
		return bet;
	}
	
	public String playRound(String input) {
		Random rand = new Random();
		int computerrand = rand.nextInt(2);
		if(input.equals("Head")) {
			if(computerrand == 0) {
				total += bet;
				return ("It is head, you win!");
			}
			else{
				total -= bet;
				return("It is tail, you lose!");
			}
		}
		else {
			if(computerrand == 0) {
				total -= bet;
				return("It is head, you lose!");
			}
			else {
				total += bet;
				return("It is tail, you win!");
			}
		}
	}
}
