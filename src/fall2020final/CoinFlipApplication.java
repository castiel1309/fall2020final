package fall2020final;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class CoinFlipApplication extends Application{
	public void start(Stage stage) {
		Group root = new Group();
		
		HBox buttons = new HBox();
		Button head = new Button("Head");
		Button tail = new Button("Tail");
		
		HBox money = new HBox();
		TextField bet = new TextField("");
		bet.setPrefWidth(200);
		TextField total = new TextField("100");
		total.setPrefWidth(200);
		TextField message = new TextField("");
		
		VBox overall = new VBox();
		
		buttons.getChildren().addAll(head, tail);
		money.getChildren().addAll(bet, total);
		overall.getChildren().addAll(buttons, money, message);
		root.getChildren().add(overall);
		
		CoinFlipChoice headchoice = new CoinFlipChoice(message, bet, total, "Head");
		head.setOnAction(headchoice);
		CoinFlipChoice tailchoice = new CoinFlipChoice(message, bet, total, "Tail");
		tail.setOnAction(tailchoice);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Coin Flip Game"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
	public static void main(String[] args) {
        Application.launch(args);
    }
}
