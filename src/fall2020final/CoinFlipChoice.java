package fall2020final;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class CoinFlipChoice implements EventHandler<ActionEvent>{
	private TextField msg;
	private TextField bet;
	private TextField total;
	private String userchoice;

	
	public CoinFlipChoice(TextField msg, TextField bet, TextField total, String userchoice) {
		this.msg = msg;
		this.bet = bet;
		this.total = total;
		this.userchoice = userchoice;
		
	}
	
	public void handle(ActionEvent e) {
		if(this.bet.getText() .equals ("")) {
			msg.setText("Please enter an amount to start");
		}
		else {
			int userbet = Integer.parseInt(this.bet.getText());
			int usertotal = Integer.parseInt(this.total.getText());
			if(userbet < 0) {
				msg.setText("Please enter an amount greater than 0");
			}
			else {
				if(userbet > usertotal) {
					msg.setText("You do not have enough money");
				}
				else {
					CoinFlipGame cfg = new CoinFlipGame(usertotal,userbet);
					String message = cfg.playRound(userchoice);
					msg.setText(message);
					total.setText("" + cfg.getTotal());
				}
			}
		}
	}
}
