package fall2020final;

public class Recursion {
	private static int count = 0;
	public static int recursiveCount(String[] words, int n) {
		
		if(n < words.length) {
			if(n % 2 != 0) {
				n -= 1;
			}
			count = recursiveCount(words, (n+2));
			if(words[n].contains("q")) {
			count++;
			System.out.println(count);
			}
		}
		System.out.println(count);
		return count;
	}
	
	public static void main(String[] args) {
		String[] test = {"Whatever", "Squirrel", "Squid", "Haha", "quad"};
		count = recursiveCount(test, 2);
		System.out.println(count);
	}
}
